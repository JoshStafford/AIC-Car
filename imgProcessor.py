import cv2

cap = cv2.VideoCapture(0)
cap1 = cv2.VideoCapture(1)
cap2 = cv2.VideoCapture(2)

def getImage():
    ret, frame0 = cap.read()
    ret, frame1 = cap1.read()
    ret, frame2 = cap2.read()

    frames = frame0 + frame1 + frame2
    
    return frames

def frame():
    frame = getImage()
    gray_image = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    return gray_image
